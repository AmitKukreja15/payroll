<?php
require_once (ROOT. 'Interfaces/monthlyPayRoll.php');
require_once (ROOT. 'Interfaces/bonusPayRoll.php');

class SalesPayRoll implements monthlyPayRoll, bonusPayRoll{
	const BONUS_PAY_DATE = 15;
	const DEFAULT_FILENAME = 'pay_dates.csv';
	protected $fileName = '';
	
	public function setFileName($fileName) {
		$this->fileName = $fileName;
	}
	
	public function execute() {
		try {
			$this->prepareAndDownloadCsv();
			echo 'CSV file is created under /files/';
		} catch(Exception $e) {
			echo 'CSV file can\'t be created';
		}
	}

	private function prepareAndDownloadCsv() {
		if(empty($this->fileName)) {
			$this->fileName = self::DEFAULT_FILENAME;
		}
		$fp = fopen('files/'.$this->fileName, 'w');
		$csvHeader = array('Month Name', 'Bonus pay date', 'Salary pay date');
		fputcsv($fp, $csvHeader);
		for($month = 1; $month <= 12; $month++) {
			fputcsv($fp, $this->getPayDates($month));
		}
	}

	public function getMonthlyPaymentDate($monthFirstDate){
		$salaryDate = date('t-m-Y', strtotime($monthFirstDate));
		$salaryPayDay = date('N', strtotime($salaryDate));
		if (in_array($salaryPayDay, array(6,7))) {
			$salaryDate = date('d-m-Y', strtotime('last friday', strtotime($salaryDate)));
		}
		return $salaryDate;
	}

	public function getBonusPaymentDate($monthFirstDate){
		$bonusDate = date('d-m-Y', strtotime($monthFirstDate. ' + '.(self::BONUS_PAY_DATE-1).' days'));
		$bonusPayDay = date('N', strtotime($bonusDate));
		if (in_array($bonusPayDay, array(6,7))) {
			$bonusDate = date('d-m-Y', strtotime('next wednesday', strtotime($bonusDate)));
		}
		return $bonusDate;
	}

	private function getPayDates($month) {
		$monthFirstDate = date('Y-'.$month.'-01');
		$monthName = date('F', mktime(0, 0, 0, $month, 10));
		$bonusDate = $this->getBonusPaymentDate($monthFirstDate);
		$salaryDate = $this->getMonthlyPaymentDate($monthFirstDate);
		return array($monthName, $bonusDate, $salaryDate);
	}
}
?>